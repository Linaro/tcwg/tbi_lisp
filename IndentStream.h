#ifndef INDENTSTREAM_H
#define INDENTSTREAM_H

#include <iostream>

// Stream class that remembers its last indent level.
class IndentStream {
public:
  IndentStream(std::ostream &out) : m_out(out) {}

  template <typename T> const IndentStream &operator<<(const T &value) const {
    m_out << value;
    return *this;
  }

  void IndentMore() { m_indent_level += m_indent_width; }

  void IndentLess() { m_indent_level -= m_indent_width; }

  void Indent() {
    std::string pad(m_indent_level, ' ');
    m_out << pad;
  }

private:
  static const unsigned m_indent_width = 4;
  unsigned m_indent_level = 0;
  std::ostream &m_out;
};

#endif // ifndef INDENTSTREAM_H
