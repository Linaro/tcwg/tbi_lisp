#ifndef SYMBOL_H
#define SYMBOL_H

#include "IndentStream.h"
#include <cassert>
#include <optional>
#include <vector>

enum SymbolType {
  UnsignedInt = 0,
  String,
};
#ifdef ENABLE_TBI
// There must be <16 for us to store it in m_value, where it has 4 bits.
static_assert(String <= 15, "Number of SymbolTypes must be <= 15");
#endif

class __attribute__((packed)) Symbol {
public:
  Symbol(Symbol &&other);
  Symbol &operator=(Symbol &&other);

  // Symbol must be move only because we have to handle string's pointer to
  // heap.
  Symbol(const Symbol &) = delete;
  Symbol &operator=(const Symbol &) = delete;

  ~Symbol();

  // Return a symbol of the same type and value but with a reference count of 1.
  // With any heap allocations duplicated so that it isn't connected to the
  // orignal.
  static Symbol From(const Symbol &other);

  // Make a new symbol of the given type, with refcount 1.
  static Symbol MakeSymbol(uint32_t value);
  static Symbol MakeSymbol(const std::string &str);

  SymbolType GetType() const;
  const char *GetTypeName() const;

  template <SymbolType ST>
  typename std::enable_if<ST == SymbolType::UnsignedInt, uint32_t>::type
  As() const;

  template <SymbolType ST>
  typename std::enable_if<ST == SymbolType::String, const std::string &>::type
  As() const;

  // Returns true if refcount was incremented, false if it was already at the
  // maximum.
  bool IncRefCount();
  unsigned DecRefCount();
  unsigned GetRefCount() const;

  // Symbols are equal if they have the same type and value. Refcount can
  // differ.
  bool operator==(const Symbol &rhs) const;
  bool operator!=(const Symbol &rhs) const;

  void dump(IndentStream &out) const;

  // A Symbol is "false" if no one references it. This allows us to use it
  // like you would a std::optional.
  operator bool() const;

private:
  // Use MakeSymbol instead.
  Symbol(SymbolType type, uintptr_t value);

  void SetType(SymbolType type);
  unsigned SetRef(unsigned count);
  uintptr_t ValueWithoutMetadata() const;
#ifdef ENABLE_TBI
  uintptr_t SetMetadata(uintptr_t value, uintptr_t lsb, uintptr_t mask);

  uintptr_t m_value;
#else
  uintptr_t m_value;
  uint8_t m_type;
  uint8_t m_refcount;
#endif
};
#ifdef ENABLE_TBI
static_assert(sizeof(Symbol) == sizeof(uintptr_t) &&
              "Symbol should be pointer sized when TBI is used.");
#else
static_assert(sizeof(Symbol) == 10 &&
              "Without TBI, Symbol should be packed to be 10 bytes.");
#endif

#endif // ifndef SMYBOL_H
