#include "Symbol.h"

#ifdef ENABLE_TBI
static const uintptr_t TYPE_LSB = 56;
static const uintptr_t REFCOUNT_LSB = 60;
static const uintptr_t TYPE_MASK = static_cast<uintptr_t>(0xf) << TYPE_LSB;
static const uintptr_t REFCOUNT_MASK = static_cast<uintptr_t>(0xf)
                                       << REFCOUNT_LSB;
static const uintptr_t METADATA_MASK = 0xff;
#endif

// Max refcount remains 15 even without TBI so that tests can be written the
// same for it on and off.
static const uintptr_t MAX_REFCOUNT = 15;

Symbol::Symbol(Symbol &&other) : m_value(other.m_value) {
  SetType(other.GetType());
  SetRef(other.GetRefCount());
  // Prevent a double delete in the event that other is a String symbol.
  other.m_value = 0;
}

Symbol::Symbol(SymbolType type, uintptr_t value) : m_value(value) {
  SetRef(1);
  SetType(type);
}

Symbol::~Symbol() {
  switch (GetType()) {
  case UnsignedInt:
    break; // Is stored directly in m_value, no destruction needed.
  case String:
    // The pointer will be nullptr if we were moved from.
    // On arm64 MacOS you can't delete a tagged pointer
    // so we have to remove metadata.
    if (uintptr_t value_only = ValueWithoutMetadata())
      delete reinterpret_cast<std::string *>(value_only);
    break;
  }
}

Symbol &Symbol::operator=(Symbol &&other) {
  SetType(other.GetType());
  m_value = other.m_value;
  SetRef(other.GetRefCount());
  // Prevent a double delete in the event that other is a String symbol.
  other.m_value = 0;
  return *this;
}

Symbol Symbol::From(const Symbol &other) {
  // Work this out first because the constructor will apply metadata to it.
  uintptr_t new_value = 0;
  switch (other.GetType()) {
  case SymbolType::UnsignedInt:
    new_value = other.m_value;
    break;
  case SymbolType::String: {
    std::string str = other.As<SymbolType::String>();
    new_value = reinterpret_cast<uintptr_t>(new std::string(str));
    break;
  }
  }
  return Symbol(other.GetType(), new_value);
}

Symbol Symbol::MakeSymbol(uint32_t value) { return Symbol(UnsignedInt, value); }

Symbol Symbol::MakeSymbol(const std::string &str) {
  return Symbol(String, reinterpret_cast<uintptr_t>(new std::string(str)));
}

template <SymbolType ST>
typename std::enable_if<ST == SymbolType::UnsignedInt, uint32_t>::type
Symbol::As() const {
  assert(GetType() == UnsignedInt);
  return static_cast<uint32_t>(ValueWithoutMetadata());
}

template <SymbolType ST>
typename std::enable_if<ST == SymbolType::String, const std::string &>::type
Symbol::As() const {
  assert(GetType() == String);
  // On arm64 MacOS you can dereference a tagged pointer
  // but you cannot delete one. So here we don't need to
  // remove the metadata.
  return *reinterpret_cast<std::string *>(m_value);
}

const char *Symbol::GetTypeName() const {
  switch (GetType()) {
  case UnsignedInt:
    return "Unsigned Int";
  case String:
    return "String";
  }
}

bool Symbol::IncRefCount() {
  unsigned current_refcount = GetRefCount();
  if (current_refcount == MAX_REFCOUNT)
    return false;
  SetRef(GetRefCount() + 1);
  return true;
}

unsigned Symbol::DecRefCount() {
  unsigned current_refcount = GetRefCount();
  return current_refcount == 0 ? 0 : SetRef(current_refcount - 1);
}

void Symbol::dump(IndentStream &out) const {
  out << "| Type - " << GetTypeName() << " | Value - ";
  switch (GetType()) {
  case UnsignedInt:
    out << As<UnsignedInt>();
    break;
  case String:
    out << "\"" << As<String>() << "\"";
    break;
  }
  out << " | References " << GetRefCount() << " |";
}

bool Symbol::operator==(const Symbol &rhs) const {
  if (GetType() != rhs.GetType())
    return false;

  switch (GetType()) {
  case SymbolType::UnsignedInt:
    return As<SymbolType::UnsignedInt>() == rhs.As<SymbolType::UnsignedInt>();
  case SymbolType::String:
    return As<SymbolType::String>() == rhs.As<SymbolType::String>();
  }
}

bool Symbol::operator!=(const Symbol &rhs) const { return !operator==(rhs); }

Symbol::operator bool() const { return GetRefCount() > 0; }

#ifdef ENABLE_TBI
SymbolType Symbol::GetType() const {
  return static_cast<SymbolType>((m_value & TYPE_MASK) >> TYPE_LSB);
}

unsigned Symbol::GetRefCount() const { return m_value >> REFCOUNT_LSB; }

uintptr_t Symbol::SetMetadata(uintptr_t value, uintptr_t lsb, uintptr_t mask) {
  m_value &= ~mask;
  m_value |= value << lsb;
  return value;
}

void Symbol::SetType(SymbolType type) {
  SetMetadata(type, TYPE_LSB, TYPE_MASK);
}

unsigned Symbol::SetRef(unsigned count) {
  return static_cast<unsigned>(SetMetadata(count, REFCOUNT_LSB, REFCOUNT_MASK));
}

uintptr_t Symbol::ValueWithoutMetadata() const {
  return m_value & ~(METADATA_MASK << TYPE_LSB);
}
#else
SymbolType Symbol::GetType() const { return static_cast<SymbolType>(m_type); }

unsigned Symbol::GetRefCount() const { return m_refcount; }

void Symbol::SetType(SymbolType type) { m_type = type; }

unsigned Symbol::SetRef(unsigned count) {
  m_refcount = count;
  return m_refcount;
}

uintptr_t Symbol::ValueWithoutMetadata() const { return m_value; }
#endif
