#ifndef FUNCTIONCALL_H
#define FUNCTIONCALL_H

#include "Symbol.h"
#include "SymbolTable.h"
#include <string>
#include <vector>

class FunctionCall {
public:
  typedef std::vector<SymbolIndex> Arguments;

  FunctionCall(std::string name, const Arguments &arguments)
      : m_name(name), m_arguments(arguments) {}

  const Arguments &GetArguments() const { return m_arguments; }

  const std::string &GetName() const { return m_name; }

  void dump(IndentStream &out, const SymbolTable &symbol_table) const {
    out << m_name << ": ";
    out.IndentMore();
    for (SymbolIndex index : m_arguments) {
      out << "\n";
      out.Indent();
      out << "[" << index << "] ";
      symbol_table.GetSymbol(index).dump(out);
    }
    out.IndentLess();
  }

private:
  std::string m_name;
  Arguments m_arguments;
};

#endif // ifndef FUNCTIONCALL_H
