#include "SymbolTable.h"

#include <cassert>

template <SymbolType ST, typename T> struct SymbolPredicate {
  bool operator()(Symbol &sym) {
    if (!sym || sym.GetType() != ST || sym.As<ST>() != m_value)
      return false;

    // Returns false if we cannot reference it any more times.
    return sym.IncRefCount();
  }

  T m_value;
};

template <SymbolType ST, typename T>
std::optional<size_t> SymbolTable::UseExistingInner(T value) {
  std::vector<Symbol>::const_iterator it = std::find_if(
      m_symbols.begin(), m_symbols.end(), SymbolPredicate<ST, T>{value});
  if (it != m_symbols.end())
    return it - m_symbols.begin();
  return {};
}

std::optional<size_t> SymbolTable::UseExisting(uint32_t value) {
  return UseExistingInner<SymbolType::UnsignedInt>(value);
}

std::optional<size_t> SymbolTable::UseExisting(const std::string &value) {
  return UseExistingInner<SymbolType::String>(value);
}

SymbolIndex SymbolTable::AddSymbol(Symbol &&sym) {
  // First try to reference an existing symbol.
  std::optional<size_t> existing_idx;
  switch (sym.GetType()) {
  case SymbolType::UnsignedInt:
    existing_idx = UseExisting(sym.As<SymbolType::UnsignedInt>());
    break;
  case SymbolType::String:
    existing_idx = UseExisting(sym.As<SymbolType::String>());
    break;
  }

  // If we found one, use it.
  if (existing_idx) {
    ++m_symbols_reused;
    if (m_verbose)
      dump(m_strm);
    return *existing_idx;
  }

  // Otherwise look for a free slot in the table.
  for (size_t idx = 0; idx < m_symbols.size(); ++idx) {
    if (!m_symbols[idx]) {
      ++m_symbol_slots_reused;
      m_symbols[idx] = std::move(sym);
      if (m_verbose)
        dump(m_strm);
      return idx;
    }
  }

  // Last resort, add a new slot and use that.
  ++m_slots_added;
  m_symbols.push_back(std::move(sym));

  if (m_verbose)
    dump(m_strm);

  return m_symbols.size() - 1;
}

const Symbol &SymbolTable::GetSymbol(SymbolIndex idx) const {
  assert(idx < m_symbols.size() && m_symbols[idx] &&
         "No symbol at this index!");
  return m_symbols[idx];
}

void SymbolTable::dump(IndentStream &out) const {
  out << "    New slots added: " << m_slots_added << "\n";
  out << "Times symbol reused: " << m_symbols_reused << "\n";
  out << "   Empty slots used: " << m_symbol_slots_reused << "\n";
  out << "  Total slots saved: " << m_symbols_reused + m_symbol_slots_reused
      << "\n";

  for (size_t idx = 0; idx < m_symbols.size(); ++idx) {
    out << "[" << idx << "]: ";
    if (m_symbols[idx])
      m_symbols[idx].dump(out);
    else
      out << "(empty slot)";
    out << "\n";
  }
  out << "\n";
}

void SymbolTable::DecRefCounts(const std::vector<SymbolIndex> indexes) {
  for (SymbolIndex idx : indexes) {
    assert(idx < m_symbols.size() && m_symbols[idx] &&
           "No symbol at this index!");
    m_symbols[idx].DecRefCount();
  }

  if (m_verbose)
    dump(m_strm);
}
