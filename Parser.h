#ifndef PARSER_H
#define PARSER_H

#include "Execute.h"
#include "FunctionCall.h"
#include "SymbolTable.h"

class Parser {
public:
  static ExecuteResult ParseAndExecute(const std::string &code,
                                       SymbolTable &symbol_table) {
    return Parser(code, symbol_table).parse_and_execute();
  }

private:
  Parser(const std::string &code, SymbolTable &symbol_table)
      : m_code(code), m_symbol_table(symbol_table), m_index(0) {}

  ExecuteResult parse_and_execute();
  std::optional<std::string> consume_function_name();
  std::optional<SymbolIndex> consume_unsigned_integer();
  void consume_whitespace();
  std::optional<SymbolIndex> consume_string();

  char peek_char();
  char consume_char();
  bool chars_left();

  // Scope guard to reset m_index to where we started parsing, in the event we
  // encounter an error. Call disarm() if the return path you're in is not an
  // error.
  struct IndexRestorer {
    IndexRestorer(Parser &parser) : m_parser(parser), m_index(parser.m_index) {}

    ~IndexRestorer() {
      if (m_restore)
        m_parser.m_index = m_index;
    }

    void disarm() { m_restore = false; }

    bool m_restore = true;
    unsigned m_index;
    Parser &m_parser;
  };

  const std::string &m_code;
  SymbolTable &m_symbol_table;
  unsigned m_index;
};

#endif // ifndef PARSER_H
