#include "Execute.h"

#include <map>
#include <numeric>
#include <sstream>
#include <functional>

// Either some value or an error string.
typedef std::variant<Symbol, std::string> FunctionResult;
// A builtin function takes some number of arguments, in the form of indexes
// into the symbol table. It returns either a value or an error string.
typedef std::function<FunctionResult(const std::vector<SymbolIndex> &,
                                     SymbolTable &)>
    BuiltinFunction;

static FunctionResult add_uint(const std::vector<SymbolIndex> &arguments,
                               SymbolTable &symbol_table) {
  return Symbol::MakeSymbol(std::accumulate(
      // Plus 1 because we'll use the first argument as the starting value.
      arguments.begin() + 1, arguments.end(),
      symbol_table.GetSymbol(*arguments.begin()).As<SymbolType::UnsignedInt>(),
      [&symbol_table](uint32_t total, SymbolIndex idx) {
        return total +
               symbol_table.GetSymbol(idx).As<SymbolType::UnsignedInt>();
      }));
}

static FunctionResult do_plus(const std::vector<SymbolIndex> &arguments,
                              SymbolTable &symbol_table) {
  if (arguments.size() == 0)
    return "+ needs at least 1 argument";

  if (std::adjacent_find(arguments.cbegin(), arguments.cend(),
                         [&symbol_table](SymbolIndex lhs, SymbolIndex rhs) {
                           return symbol_table.GetSymbol(lhs).GetType() !=
                                  symbol_table.GetSymbol(rhs).GetType();
                         }) != arguments.end())
    return "All arguments to + must be the same type";

  switch (symbol_table.GetSymbol(arguments[0]).GetType()) {
  case SymbolType::UnsignedInt:
    return add_uint(arguments, symbol_table);
  case SymbolType::String:
    return Symbol::MakeSymbol(std::accumulate(
        arguments.begin(), arguments.end(), std::string{},
        [&symbol_table](std::string str, SymbolIndex idx) {
          return str + symbol_table.GetSymbol(idx).As<SymbolType::String>();
        }));
  }
}

ExecuteResult execute(const FunctionCall &fn, SymbolTable &symbol_table) {
  if (fn.GetName() != "+") {
    std::stringstream strm;
    strm << "Unknown function name \"" << fn.GetName() << "\"";
    return strm.str();
  }

  // Run the function.
  FunctionResult result_or_err = (do_plus)(fn.GetArguments(), symbol_table);
  // If it failed, return the error.
  if (std::holds_alternative<std::string>(result_or_err))
    return std::get<std::string>(result_or_err);

  // We're now done with the arguments.
  symbol_table.DecRefCounts(fn.GetArguments());

  Symbol result = std::move(std::get<Symbol>(result_or_err));
  return symbol_table.AddSymbol(std::move(result));
}
