#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

#include "IndentStream.h"
#include "Parser.h"
#include "SymbolTable.h"

static void test_code(const char *code, Symbol expected) {
  IndentStream istrm(std::cout);
  SymbolTable symbol_table(istrm, false);

  ExecuteResult result = Parser::ParseAndExecute(code, symbol_table);
  if (std::holds_alternative<std::string>(result)) {
    // Seems like the least worst way to surface the error.
    std::cout << std::get<std::string>(result).c_str();
    FAIL("Unexpected error.");
  }

  const Symbol &got = symbol_table.GetSymbol(std::get<SymbolIndex>(result));

  if (expected != got) {
    IndentStream strm(std::cout);
    strm << "Expected: ";
    expected.dump(strm);
    strm << "\n";
    strm << "     Got: ";
    got.dump(strm);
    strm << "\n";
  }

  CHECK(expected == got);
}

TEST_CASE("code samples") {
  // The basics.
  test_code("(+ 1)", Symbol::MakeSymbol(1));
  test_code("   (     + 1    )   ", Symbol::MakeSymbol(1));
  test_code("(+ 1 1)", Symbol::MakeSymbol(2));
  test_code("(+ 2 3 4)", Symbol::MakeSymbol(9));
  test_code("(+ (+ 2) (+ 3))", Symbol::MakeSymbol(5));
  // Our ints are 32 bit unsigned.
  test_code("(+ 4294967295 1)", Symbol::MakeSymbol(0));

  test_code("(+ \"a\")", Symbol::MakeSymbol("a"));
  test_code("(+ \"a space\")", Symbol::MakeSymbol("a space"));
  test_code("(+ \"a\" \"a\")", Symbol::MakeSymbol("aa"));
  test_code("(+ \"a\" \"b\" \"c\")", Symbol::MakeSymbol("abc"));
  test_code("(+ (+ \"a\") (+ \"b\"))", Symbol::MakeSymbol("ab"));
}

static void test_code_error(const char *code, const char *expected_err) {
  IndentStream istrm(std::cout);
  SymbolTable symbol_table(istrm, false);

  ExecuteResult result = Parser::ParseAndExecute(code, symbol_table);
  if (std::holds_alternative<SymbolIndex>(result)) {
    FAIL("Expected code to fail.");
  }

  const std::string &err = std::get<std::string>(result);
  CHECK(expected_err == err);
}

TEST_CASE("Code errors") {
  test_code_error("", "Function at index 0 does not begin with '('");
  test_code_error("(", "Expected function name at index 1");
  // Not ideal
  test_code_error("(+", "Expected function name at index 1");
  test_code_error("()", "Expected function name at index 1");
  test_code_error("(  )", "Expected function name at index 3");
  test_code_error("(?)", "Unknown function name \"?\"");
  test_code_error("(+ ", "Unexpected end of code");
  test_code_error("(+ 1", "Unexpected end of code");
  // Not ideal
  test_code_error("(+ \"foo", "Could not parse argument at index 3");
  test_code_error("(()", "Expected function name at index 1");
}

TEST_CASE("Function param types") {
  test_code("(+ 1 2 3)", Symbol::MakeSymbol(6));
  test_code("(+ \"a\" \"b\" \"c\")", Symbol::MakeSymbol("abc"));
  test_code_error("(+ 1 \"a\")", "All arguments to + must be the same type");
}

static void test_symbol_reuse(const char *code, size_t expected_added,
                              size_t expected_reused,
                              size_t expected_resused_slots) {
  IndentStream istrm(std::cout);
  SymbolTable symbol_table(istrm, false);

  ExecuteResult result = Parser::ParseAndExecute(code, symbol_table);
  if (std::holds_alternative<std::string>(result)) {
    FAIL("Unexpected error.");
  }

  CHECK(expected_added == symbol_table.GetSlotsAdded());
  CHECK(expected_reused == symbol_table.GetSymbolsReused());
  CHECK(expected_resused_slots == symbol_table.GetSymbolSlotsReused());
}

TEST_CASE("Symbol reuse") {
  // 1 new for argument 1. Then it is freed and we reuse the slot for the
  // result of 1.
  test_symbol_reuse("(+ 1)", 1, 0, 1);
  // 5 unique arguments, 5 slots added. 1 reused for the result.
  test_symbol_reuse("(+ 1 2 3 4 5)", 5, 0, 1);
  // 1 new for argument 1, reused for 2nd argument. Freed then slot reused
  // for result of 2.
  test_symbol_reuse("(+ 1 1)", 1, 1, 1);
  test_symbol_reuse("(+ \"f\" \"f\")", 1, 1, 1);
  // Added 2, 1 for the 1s and 1 for the result of (+ 1 1) later.
  // Reused the 1 3 times because there are 2 arguments of 1 and the result
  // of the innermomst function is also 1.
  // The 2nd slot is freed and reused a bunch as we come back up the tree,
  // and for the outer function we reuse the first slot.
  test_symbol_reuse("(+ (+ 1 (+ 1 (+ 1))))", 2, 3, 2);
  // One slot is used for a 1 that is reused 14 times (refcount = 15).
  // We make a second 1 since we hit the refcount limit. This one is reused
  // once also. Finally we reuse slot 0 for the result.
  test_symbol_reuse("(+ 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1)", 2, 15, 1);
  // As we execute as we go, we should have as many slots as the deepest
  // branch adds. So the first path has a depth of 3, those 3 slots get reused
  // for the 2 arguments later and 6 results.
  test_symbol_reuse("\
  (+\
    (+ 2\
      (+ 3\
        (+ 4)\
      )\
    )\
    (+ 2\
      (+ 3)\
    )\
  )",
                    3, 0, 8);
}
