#include "Parser.h"

#include <sstream>

char Parser::peek_char() { return m_code[m_index]; }

char Parser::consume_char() { return m_code[m_index++]; }

bool Parser::chars_left() { return m_index < m_code.size(); }

ExecuteResult Parser::parse_and_execute() {
  std::stringstream strm;
  consume_whitespace();

  if (peek_char() != '(') {
    strm << "Function at index " << m_index << " does not begin with '('";
    return strm.str();
  }
  consume_char();

  consume_whitespace();

  std::optional<std::string> function_name = consume_function_name();
  if (!function_name) {
    strm << "Expected function name at index " << m_index;
    return strm.str();
  }

  consume_whitespace();

  FunctionCall::Arguments arguments;
  while (chars_left() && peek_char() != ')') {
    // If the argument is another function call.
    if (peek_char() == '(') {
      // Recurse to parse it.
      ExecuteResult symbol_or_err = parse_and_execute();
      if (std::holds_alternative<std::string>(symbol_or_err))
        return symbol_or_err;

      arguments.push_back(std::get<SymbolIndex>(symbol_or_err));
    } else {
      // Must be an argument of some type.
      if (std::optional<SymbolIndex> unsigned_int_arg =
              consume_unsigned_integer())
        arguments.push_back(*unsigned_int_arg);
      else {
        if (std::optional<SymbolIndex> string_arg = consume_string())
          arguments.push_back(*string_arg);
        else {
          strm << "Could not parse argument at index " << m_index;
          return strm.str();
        }
      }
    }
    consume_whitespace();
  }

  if (!chars_left())
    return "Unexpected end of code";
  // The closing )
  consume_char();

  return execute(FunctionCall(*function_name, arguments), m_symbol_table);
}

std::optional<SymbolIndex> Parser::consume_unsigned_integer() {
  size_t chars_used = 0;
  unsigned value = 0;

  try {
    value =
        static_cast<unsigned>(std::stoul(&m_code[m_index], &chars_used, 10));
  } catch (const std::invalid_argument &) {
    return {};
  }

  m_index += chars_used;
  return m_symbol_table.AddSymbol(value);
}

void Parser::consume_whitespace() {
  while ((peek_char() == ' ') || (peek_char() == '\n'))
    consume_char();
}

std::optional<std::string> Parser::consume_function_name() {
  IndexRestorer restorer(*this);
  std::string chars("");

  while (peek_char() != ' ' && peek_char() != '(' && peek_char() != ')') {
    chars += consume_char();
    if (!chars_left())
      return {};
  }

  if (!chars.size())
    return {};

  restorer.disarm();
  return chars;
}

std::optional<SymbolIndex> Parser::consume_string() {
  IndexRestorer restorer(*this);

  if (consume_char() != '"')
    return {};

  std::string str;
  while (chars_left() && peek_char() != '"') {
    str += consume_char();
  }

  // TODO: signal lack of closing " here
  if (!chars_left())
    return {};
  // The closing "
  consume_char();

  restorer.disarm();
  return m_symbol_table.AddSymbol(str);
}
