# Top Byte Ignore LISP

This repo contains a very limited, not at all robust, lisp style expression evaluator.
It is explained in much more detail in this [article on Linaro's blog](https://www.linaro.org/blog/top-byte-ignore-for-fun-and-memory-savings/).

The twist is, it uses AArch64's Top Byte Ignore feature.

Top Byte Ignore (TBI) is a feature of Armv8-a AArch64 that allows software to use unused pointer bits to store data, without hiding them from the hardware. Reducing memory use, software overhead and programmer frustration.

Here we use it to compress the size of the symbol table by storing symbol type and reference count in a single pointer sized value. Achieving 8 bytes per symbol vs. 10 for a conventional packed class.

We combine this with the usual reference counting techniques to limit the maximum size of the symbol table at runtime. The TBI specifics are mostly in [Symbol.cpp](Symbol.cpp).

## Usage

`tbi_lisp` takes one argument, which is the source code to evaluate.

```
$ ./tbi_lisp "(+ 1 1 1 1)"
    New slots added: 1
Times symbol reused: 3
   Empty slots used: 1
  Total slots saved: 4
[0]: | Type - Unsigned Int | Value - 4 | References 1 |
```

Statistics are shown about how many symbol table slots were added, reused and saved vs. not attempting to save and reuse slots. The result of the program is the 1 remaining symbol in the table.

To run under QEMU user mode emulation, you will need to provide a library path:
```
$ qemu-aarch64 -L /usr/aarch64-linux-gnu/ ./tbi_lisp "(+ 1 2 3)"
    New slots added: 3
Times symbol reused: 0
   Empty slots used: 1
  Total slots saved: 1
[0]: | Type - Unsigned Int | Value - 6 | References 1 |
[1]: (empty slot)
[2]: (empty slot)
```

## Requirements

* CMake
* Ninja or Make (as you prefer)
* A C++17 compiler
* If you want to use the TBI features, one of:
  * An Arm64 Mac.
  * An AArch64 Linux machine.
  * QEMU emulation of the former (qemu-system).
  * QEMU user mode emulation (qemu-aarch64).
* Otherwise, any host should work.

## Building

To compile to run on AArch64 with TBI:
```
cmake . -G Ninja
ninja
```

To compile without TBI on AArch64, or to compile for any other architecture than
AArch64:
```
cmake . -G Ninja -DENABLE_TBI=OFF
ninja
```

If you are compiling to run with qemu-aarch64 it will be something like this. Adjust as needed
for your system's package names.
```
cmake . -G Ninja -DCMAKE_CXX_COMPILER=aarch64-none-linux-gnu-g++
ninja
```

To run with `qemu-aarch64` you will need to use the `-L <path to library folder`
option, or add the CMake option `-DCMAKE_CXX_FLAGS=-static` to link statically.

## Tests

Tests are written using [doctest](https://github.com/doctest/doctest).

```
ninja && ninja test
```

## Implementation Notes

"Any sufficiently complicated C or Fortran program contains an ad hoc, informally-specified, bug-ridden, slow implementation of half of Common Lisp."

This is a tiny fraction of that, just enough to demonstrate how top byte ignore can be used.

The key thing to know to read the source is that Symbols are either:
* 32 bit unsigned integers.
* Pointers to some string on the heap.

The only function is "+" and its arguments must all be of the same type.
