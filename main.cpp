#include "IndentStream.h"
#include "Parser.h"
#include "SymbolTable.h"

int main(int argc, char *argv[]) {
  // clang-format off
  /* A few example programs.

  Grows to N slots then reuses all the free ones.
  (+ 1 (+ 2 (+ 3 (+ 4 (+ 5 (+ 6))))))

  Only ever has 1 symbol.
  (+ (+ (+ (+ (+ (+ 1))))))

  Uses 1 slot for all the 1s and 1 for the results.
  (+ 1 (+ 1 (+ 1 (+ 1))))

  Same for strings.
  (+ \"foo\" (+ \"foo\" (+ \"foo\" (+ \"foo\"))))

  Since the result of the inner function is the same as the first
  parameter, we can reuse the symbol.
  (+ 5 (+ 4 1))

  Because we execute as we parse we only need as many slots
  as there are unique symbols in the deepest branch.
  So this allocates 3 slots first and the second branch
  reuses them.
  (+\
    (+ 1\
      (+ 2 2 2 2 2 \
        (+ 3)\
      )\
    )\
    (+ 4\
      (- 5 2)\
    )\
  )

  This should reference the first 1 16 times then the last 1 once.
  (+ 1 (+ 1 (+ 1 (+ 1 (+ 1 (+ 1 (+ 1 (+ 1 (+ 1 (+ 1 (+ 1 (+ 1 (+ 1 (+ 1 (+ 1 (+ 1 (+ 1)))))))))))))))))))
  */
  // clang-format on

  if (argc != 2) {
    std::cerr << "Expected exactly one argument, which is the source code.\n";
    return 1;
  }

  IndentStream istrm(std::cout);
  SymbolTable symbol_table(istrm, /*verbose=*/false);

  ExecuteResult result = Parser::ParseAndExecute(argv[1], symbol_table);
  if (std::holds_alternative<std::string>(result)) {
    std::cerr << "Error: " << std::get<std::string>(result) << std::endl;
    return 1;
  }

  // The only valid symbol in the table will be the result.
  symbol_table.dump(istrm);

  return 0;
}
