#ifndef SYMBOLTABLE_H
#define SYMBOLTABLE_H

#include "IndentStream.h"
#include "Symbol.h"
#include <algorithm>
#include <type_traits>
#include <vector>

typedef unsigned SymbolIndex;

class SymbolTable {
public:
  // Disable copy constructor because we always want to pass this by reference.
  SymbolTable(const SymbolTable &) = delete;

  // Set verbose true to print after every change to the symbol table.
  SymbolTable(IndentStream &strm, bool verbose)
      : m_strm(strm), m_verbose(verbose) {}

  unsigned AddSymbol(uint32_t value) {
    return AddSymbol(Symbol::MakeSymbol(value));
  }

  unsigned AddSymbol(std::string str) {
    return AddSymbol(Symbol::MakeSymbol(str));
  }

  unsigned AddSymbol(Symbol &&sym);

  const Symbol &GetSymbol(SymbolIndex idx) const;

  void dump(IndentStream &out) const;

  void DecRefCounts(const std::vector<SymbolIndex> indexes);

  size_t GetSlotsAdded() const { return m_slots_added; }
  size_t GetSymbolsReused() const { return m_symbols_reused; }
  size_t GetSymbolSlotsReused() const { return m_symbol_slots_reused; }

private:
  std::optional<size_t> UseExisting(uint32_t value);
  std::optional<size_t> UseExisting(const std::string &value);

  template <SymbolType ST, typename T>
  std::optional<size_t> UseExistingInner(T value);

  IndentStream &m_strm;
  bool m_verbose;
  std::vector<Symbol> m_symbols;
  size_t m_slots_added = 0;
  size_t m_symbols_reused = 0;
  size_t m_symbol_slots_reused = 0;
};

#endif // ifndef SYMBOLTABLE_H
