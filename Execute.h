#ifndef EXECUTE_H
#define EXECUTE_H

#include "FunctionCall.h"
#include "SymbolTable.h"

#include <variant>

// Either a result or an eror message.
typedef std::variant<SymbolIndex, std::string> ExecuteResult;
ExecuteResult execute(const FunctionCall &fn, SymbolTable &symbol_table);

#endif // ifndef EXECUTE_H
